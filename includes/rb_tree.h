/* ********************************************************************************************** */
/*                                                                                                */
/*                                                        ################  ###           ###     */
/*                                                        ################  ####         ####     */
/*   rb_tree.h                                                   ##         ## ##       ## ##     */
/*                                                      #######  ##  ###### ##  ## ### ##  ## ##  */
/*                                                     ########  ##  ###### ##  ##  #  ##  ## ##  */
/*   By: Cervac Petru <petru.cervac@gmail.com>        ##     ##  ##  ##     ##   ##   ##   ##     */
/*                                                      :::::##::##::##:::::##:::##:::##:::##:::  */
/*                                                      :::::##::##::##:::::##::::##:##::::##:::  */
/*   Created: 2017/08/05 00:55:14 by Cervac                  ##  ##  ##     ##    ## ##    ##     */
/*   Updated: 2017/08/05 02:57:11 by Cervac Petru             ##    ##      ##     ###     ##     */
/*                                                              ####        ##      #      ##     */
/*                                                                                                */
/* ********************************************************************************************** */

#ifndef RB_node_H
# define RB_node_H

# include <stdlib.h>


typedef enum			e_rb_color
{
	RB_BLACK,
	RB_RED
}						t_rb_color;

typedef enum			e_rb_direction
{
	RB_LEFT,
	RB_RIGHT
}						t_rb_direction;

typedef struct			s_rb_node
{
	struct s_rb_node	*parent;
	struct s_rb_node	*left;
	struct s_rb_node	*right;
	void				*data;
	size_t				size;
	t_rb_color			color;
}						t_rb_node;
typedef t_rb_node		t_rb_tree;

t_rb_node				*rb_create_node(void *data, size_t size);
void					rb_insert(t_rb_node **root, void *data, size_t size,
								  int (*cmpf)(void *, void *));
void					*ft_memcpy(void *mem1, void *mem2, size_t size);

#endif