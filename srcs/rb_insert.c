/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rb_insert.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pcervac <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 06:01:17 by pcervac           #+#    #+#             */
/*   Updated: 2017/08/06 06:01:20 by pcervac          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rb_tree.h"
#include <stdio.h>
#define BROTHER(p, c)	((p)->left == (c) ? (p)->right : (p)->left)
#define UNCLE(g, p)		BROTHER(g, p)

#define LEFT(root)			((root)->left)
#define RIGHT(root)			((root)->right)

#define IS_LEFT_CHILD(parent, child)	((parent)->left == (child))
#define IS_RIGHT_CHILD(parent, child)	((parent)->right == (child))

#define GRANDPARENT(p)		((p)->parent)

#define GET_COLOR(node)		(((node) != NULL) ? (node)->color : RB_BLACK)

#define DO_NONE	0;

static t_rb_node	*left_rotate(t_rb_node *parent, t_rb_node *child)
{
	t_rb_node	*temp;

	temp = child->right;
	child->right = parent;
	parent->left = temp;
	child->parent = parent->parent;
	parent->parent = child;
	if (child->parent != NULL)
	{
		if (IS_LEFT_CHILD(child->parent, parent))
			child->parent->left = child;
		else
			child->parent->right = child;
	}
	if (parent->left != NULL)
		parent->left->parent = parent;
	return (child);
}

static t_rb_node	*right_rotate(t_rb_node *parent, t_rb_node *child)
{
	t_rb_node	*temp;

	temp = child->left;
	child->left = parent;
	parent->right = temp;
	child->parent = parent->parent;
	parent->parent = child;
	if (child->parent != NULL)
	{
		if (IS_LEFT_CHILD(child->parent, parent))
			child->parent->left = child;
		else
			child->parent->right = child;
	}
	if (parent->right != NULL)
		parent->right->parent = parent;
	return (child);
}

static t_rb_node	*rb_add_node(t_rb_node **root, void *data, size_t size,
		int (*cmpf)(void *, void *))
{
	static t_rb_node	*parent = NULL;

	if (*root != NULL)
	{
		parent = *root;
		if (cmpf((*root)->data, data) > 0)
			return (rb_add_node(&LEFT(*root), data, size, cmpf));
		return (rb_add_node(&RIGHT(*root), data, size, cmpf));
	}
	*root = rb_create_node(data, size);
	(*root)->parent = parent;
	if (parent == NULL)
		(*root)->color = RB_BLACK;
	return (*root);
}

static t_rb_node	*rb_arrage(t_rb_node *child)
{
	t_rb_node	*grp;
	t_rb_node	*parent;

	if (NULL == (parent = child->parent))
		return (child);
	if (NULL == (grp = parent->parent))
		return (parent);
	if (parent->color != RB_RED || child->color != RB_RED)
		return (rb_arrage(grp));
	if (RB_RED == GET_COLOR(UNCLE(grp, parent)))
	{
		parent->color = RB_BLACK;
		UNCLE(grp, parent)->color = RB_BLACK;
		grp->parent != NULL ? grp->color = RB_RED : DO_NONE;
		return (rb_arrage(grp));
	}
	parent->color = RB_BLACK;
	grp->color = RB_RED;
	if (IS_LEFT_CHILD(grp, parent) && IS_LEFT_CHILD(parent, child))
		return (rb_arrage(left_rotate(grp, parent)));
	else if (IS_LEFT_CHILD(grp, parent) && IS_RIGHT_CHILD(parent, child))
		return (rb_arrage(left_rotate(grp, right_rotate(parent, child))));
	else if (IS_RIGHT_CHILD(grp, parent) && IS_RIGHT_CHILD(parent, child))
		return (rb_arrage(right_rotate(grp, parent)));
	return (rb_arrage(right_rotate(grp, left_rotate(parent, child))));
}

void	rb_insert(t_rb_node **root, void *data, size_t size,
		int (*cmpf)(void *, void *))
{
	*root = rb_arrage(rb_add_node(root, data, size, cmpf));
}
