/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pcervac <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 06:00:33 by pcervac           #+#    #+#             */
/*   Updated: 2017/08/06 06:00:36 by pcervac          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "rb_tree.h"

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

void	print_tree(t_rb_tree *tree, int spaces)
{
	if (tree != NULL)
	{
		char *color = tree->color == RB_RED ? RED : "";
		for (int i = 0; i < spaces; i++)
			printf(" ");
		printf("%s%d%s", color, *((int*)tree->data), RESET);
		if (tree->parent == NULL)
			printf(" - root\n");
		else if (tree->parent->left == tree)
			printf(" - left\n");
		else
			printf(" - right\n");
		print_tree(tree->left, spaces + 4);
		print_tree(tree->right, spaces + 4);
	}
}

int compare(void *val1, void *val2)
{
	return (*(int*)val1 - *(int*)val2);
}

int	main(void)
{
	int data;
	t_rb_tree	*tree = NULL;

	for (int i = 0; i < 10; i++)
	{
		data = rand() % 10;
		rb_insert(&tree, &data, sizeof(data), &compare);
	}
	print_tree(tree, 0);
	return (0);
}
