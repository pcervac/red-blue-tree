/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rb_create_node.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pcervac <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/06 06:00:53 by pcervac           #+#    #+#             */
/*   Updated: 2017/08/06 06:00:56 by pcervac          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

#include "rb_tree.h"

t_rb_node	*rb_create_node(void *data, size_t size)
{
	t_rb_node	*node;

	if (NULL == (node = (t_rb_node*)malloc(sizeof(t_rb_node))))
		return (NULL);
	node->data = ((data != NULL) ? memcpy(malloc(size), data, size) : NULL);
	node->size = ((data != NULL) ? size : 0);
	node->parent = NULL;
	node->left = NULL;
	node->right = NULL;
	node->color = RB_RED;
	return (node);
}
