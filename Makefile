# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pcervac <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/08/05 11:48:04 by pcervac           #+#    #+#              #
#    Updated: 2017/08/05 11:52:32 by pcervac          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = test
SRCS = srcs/main.c \
	  srcs/rb_create_node.c \
	  srcs/rb_insert.c
OBJ = ${notdir ${SRC:.c=.o}}

INC = -I includes

CC = gcc
CFLAGS = -Wall -Wextra -Werror

all: ${NAME} run

${NAME}:
	@${CC} ${CFLAGS} ${SRCS} ${INC} -o ${NAME}

clean:
	@rm -f ${OBJ}

fclean: clean
	@rm -f ${NAME}

re: fclean all

run: ${NAME}
	@./${NAME}
